#ChatTest

A project demonstrating the handling of chat messages to identify common keywords such as @mentions, (emoticons), and links.

Its ability to recognise keywords in a chat message is extensible by implementing new matching keyword classes that conform to the KeywordEntity protocol. For example #hashtags

The project includes some demo chat messages and also supports adding new Chat Messages using the + button to prove the functionality of matching the registered keywords.

##Usage

This project must be opened via the `ChatTest.xcworkspace` file located in the root directory.

For easy demonstration purposes the `Pods/` directory has been included in source control so that the project can be immediately run without any intermediate steps. Cocoapods is used in this project solely for the purpose of unescaping HTML entities in link title strings.

##Author

Daniel Bowden

[https://bitbucket.org/danielbowden/](https://bitbucket.org/danielbowden/)

[https://github.com/danielbowden](https://github.com/danielbowden)

[http://www.danielbowden.com.au](http://www.danielbowden.com.au)
