//
//  ChatMessageTableViewCell.h
//  ChatTest
//
//  Created by Daniel Bowden on 14/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatMessage;

@interface ChatMessageTableViewCell : UITableViewCell

+ (NSString *)cellIdentifier;
+ (CGFloat)estimatedRowHeight;

- (void)populateWithMessage:(ChatMessage *)message;

@end
