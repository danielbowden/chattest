//
//  ChatMessageTableViewCell.m
//  ChatTest
//
//  Created by Daniel Bowden on 14/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "ChatMessageTableViewCell.h"

#import "ChatMessage.h"

@interface ChatMessageTableViewCell ()

@property (nonatomic, strong) IBOutlet UILabel *messageLabel;

@end

static ChatMessageTableViewCell *prototypeStaticCell = nil;

@implementation ChatMessageTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]))
    {
        self.messageLabel = [[UILabel alloc] init];
        self.messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.messageLabel.textColor = [UIColor grayColor];
        self.messageLabel.font = [UIFont systemFontOfSize:16.0];
        
        [self.contentView addSubview:self.messageLabel];
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [self setupConstraints];
    }
    
    return self;
}

- (void)setupConstraints
{
    NSDictionary *viewDict = @{@"messageLabel" : self.messageLabel};
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[messageLabel]-10-|" options:0 metrics:0 views:viewDict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[messageLabel]-10-|" options:0 metrics:0 views:viewDict]];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}

- (void)populateWithMessage:(ChatMessage *)message
{
    self.messageLabel.text = message.plainText;
}

- (NSString *)reuseIdentifier
{
    return [[self class] cellIdentifier];
}

+ (NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

+ (CGFloat)estimatedRowHeight
{
    return 44.0;
}

@end
