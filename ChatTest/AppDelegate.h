//
//  AppDelegate.h
//  ChatTest
//
//  Created by Daniel Bowden on 09/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

