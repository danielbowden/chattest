//
//  Emoticon.m
//  ChatTest
//
//  Created by Daniel Bowden on 10/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "Emoticon.h"

@interface Emoticon ()

@property (nonatomic, copy) NSString *name;

+ (NSString *)emoticonName:(NSString *)originalString;

@end

static NSInteger const maxNameLength = 15;

@implementation Emoticon

@synthesize plainString;

+ (BOOL)meetsRequirements:(NSString *)aString
{
    BOOL valid = NO;
    
    NSMutableCharacterSet *validCharacterSet = [NSMutableCharacterSet alphanumericCharacterSet];

    if ([aString hasPrefix:@"("] && [aString hasSuffix:@")"])
    {
        aString = [self emoticonName:aString];
        valid = (aString.length <= maxNameLength && [aString rangeOfCharacterFromSet:validCharacterSet.invertedSet].location == NSNotFound);
    }
    
    return valid;
}

- (instancetype)initWithKeywordString:(NSString *)aString
{
    if ((self = [super init]))
    {
        self.plainString = aString;
        self.name = [[self class] emoticonName:aString];
    }
    
    return self;
}

- (id)serialize
{
    return self.name;
}

#pragma mark - Private

+ (NSString *)emoticonName:(NSString *)originalString
{
    return [originalString substringWithRange:NSMakeRange(1, originalString.length-2)];
}

@end
