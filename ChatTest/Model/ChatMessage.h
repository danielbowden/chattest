//
//  ChatMessage.h
//  ChatTest
//
//  Created by Daniel Bowden on 9/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatMessage : NSObject

@property (nonatomic, copy) NSString *plainText;

- (instancetype)initWithMessage:(NSString *)message;
- (NSString *)serialize;

@end
