//
//  Mention.m
//  ChatTest
//
//  Created by Daniel Bowden on 09/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "Mention.h"

@interface Mention ()

@property (nonatomic, copy) NSString *username;

+ (NSString *)username:(NSString *)originalString;

@end

@implementation Mention

@synthesize plainString;

+ (BOOL)meetsRequirements:(NSString *)aString
{
    BOOL valid = NO;
    
    NSMutableCharacterSet *validCharacterSet = [NSMutableCharacterSet letterCharacterSet];
    [validCharacterSet addCharactersInString:@"_"]; //usernames could contain underscores
    
    if ([aString hasPrefix:@"@"])
    {
        aString = [self username:aString];
        valid = ([aString rangeOfCharacterFromSet:validCharacterSet.invertedSet].location == NSNotFound);
    }
    
    return valid;
}

- (instancetype)initWithKeywordString:(NSString *)aString
{
    if ((self = [super init]))
    {
        self.plainString = aString;
        self.username = [[self class] username:aString];
    }
    
    return self;
}

- (id)serialize
{
    return self.username;
}

#pragma mark - Private

+ (NSString *)username:(NSString *)originalString
{
    return [originalString substringFromIndex:1];
}

@end
