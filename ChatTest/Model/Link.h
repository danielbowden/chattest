//
//  Link.h
//  ChatTest
//
//  Created by Daniel Bowden on 10/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KeywordEntity.h"

@interface Link : NSObject <KeywordEntity>

- (void)requestPageTitle:(void (^)(NSString* pageTitle))success failure:(void (^)(NSError *error))failure;

@end
