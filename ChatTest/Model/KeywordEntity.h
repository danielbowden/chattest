//
//  KeywordEntity.h
//  ChatTest
//
//  Created by Daniel Bowden on 09/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
    A KeywordEntity can describe any item that is a keyword that you would like to match inside a ChatMessage. Currently we only match for @Mentions, (Emoticons) and Links but in the future this could be anything, such as Hashtags, as long as it conforms to this KeywordEntity protocol.
 */

@protocol KeywordEntity <NSObject>

@required
@property (nonatomic, copy) NSString *plainString;


/**
 This method should be implemented by your class to test if a given string meets its requirements for a match.
 */
+ (BOOL)meetsRequirements:(NSString *)string;


/**
 Initialises an object with the specified string.
 This is the designated initialiser.
 */
- (instancetype)initWithKeywordString:(NSString *)string;


/**
 Must return a serializable object describing the instance to be used as part of the JSON output of a ChatMessage.
 
 As per NSJSONSerialization, the object must have the following properties:
 - Top level object is an NSArray or NSDictionary
 - All objects are NSString, NSNumber, NSArray, NSDictionary, or NSNull
 - All dictionary keys are NSStrings
 - NSNumbers are not NaN or infinity
 */
- (id)serialize;

@end
