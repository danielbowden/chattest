//
//  ChatMessage.m
//  ChatTest
//
//  Created by Daniel Bowden on 9/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "ChatMessage.h"

#import "KeywordEntity.h"
#import "Mention.h"
#import "Emoticon.h"
#import "Link.h"

@interface ChatKeywordType : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) Class class;

@end

@interface ChatMessage ()

@property (nonatomic, strong) NSMutableArray<ChatKeywordType *> *validKeywordTypes;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSMutableArray *> *matchedKeywords;

- (void)registerKeywordType:(NSString *)name class:(Class)keywordClass;
- (void)processMessage:(NSString *)message;
- (void)checkKeywordMatch:(NSString *)word;
- (void)addMatch:(id<KeywordEntity>)match forKeywordType:(ChatKeywordType *)keywordType;

@end

@implementation ChatMessage

- (instancetype)initWithMessage:(NSString *)message
{
    if ((self = [super init]))
    {
        self.plainText = message;
        
        [self registerKeywordType:@"mentions" class:[Mention class]];
        [self registerKeywordType:@"emoticons" class:[Emoticon class]];
        [self registerKeywordType:@"links" class:[Link class]];
        
        self.matchedKeywords = [NSMutableDictionary dictionary];
        [self processMessage:message];
    }
    
    return self;
}

- (NSString *)serialize
{
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    
    for (NSString *keywordName in self.matchedKeywords)
    {
        NSArray *results = [self.matchedKeywords[keywordName] valueForKeyPath:@"serialize"];
        
        jsonDict[keywordName] = [NSMutableArray arrayWithArray:results];
    }
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:&error];
    
    if (jsonData != nil && !error)
    {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    else
    {
        return @"No valid JSON";
    }
}

#pragma mark - Private methods

- (void)registerKeywordType:(NSString *)name class:(Class)keywordClass
{
    NSParameterAssert([keywordClass conformsToProtocol:@protocol(KeywordEntity)]);
    
    if (!self.validKeywordTypes)
    {
        self.validKeywordTypes = [NSMutableArray array];
    }
    
    ChatKeywordType *type = [[ChatKeywordType alloc] init];
    type.name = name;
    type.class = keywordClass;
    
    [self.validKeywordTypes addObject:type];
}

- (void)processMessage:(NSString *)message
{
    NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    for (NSString *word in [message componentsSeparatedByCharactersInSet:characterSet])
    {
        if (word.length > 1) //nothing will match a 1 character word. I, a, etc
        {
            [self checkKeywordMatch:word];
        }
    }
}

- (void)checkKeywordMatch:(NSString *)word
{
    for (ChatKeywordType *keywordType in self.validKeywordTypes)
    {
        if ([keywordType.class respondsToSelector:@selector(meetsRequirements:)] && [keywordType.class meetsRequirements:word])
        {
            if ([keywordType.class instancesRespondToSelector:@selector(initWithKeywordString:)])
            {
                id<KeywordEntity> match = [[keywordType.class alloc] initWithKeywordString:word];
                
                [self addMatch:match forKeywordType:keywordType];
            }
            
            continue;
        }
    }
}

- (void)addMatch:(id<KeywordEntity>)match forKeywordType:(ChatKeywordType *)keywordType
{
    if (self.matchedKeywords[keywordType.name] != nil)
    {
        [self.matchedKeywords[keywordType.name] addObject:match];
    }
    else
    {
        self.matchedKeywords[keywordType.name] = [NSMutableArray arrayWithObject:match];
    }
}

@end

@implementation ChatKeywordType

@end
