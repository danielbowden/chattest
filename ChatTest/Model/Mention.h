//
//  Mention.h
//  ChatTest
//
//  Created by Daniel Bowden on 09/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KeywordEntity.h"

@interface Mention : NSObject <KeywordEntity>

@end
