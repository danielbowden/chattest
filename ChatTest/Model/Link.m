//
//  Link.m
//  ChatTest
//
//  Created by Daniel Bowden on 10/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "Link.h"

#import "GTMNSString+HTML.h"

@interface Link ()

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, copy) NSString *title;

@end

@implementation Link

@synthesize plainString;

+ (BOOL)meetsRequirements:(NSString *)aString
{
    NSError *error = nil;
    NSDataDetector *detector = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:&error];

    return ([detector numberOfMatchesInString:aString options:0 range:NSMakeRange(0, aString.length)] > 0);
}

- (instancetype)initWithKeywordString:(NSString *)aString
{
    if ((self = [super init]))
    {
        self.plainString = aString;
        self.url = [NSURL URLWithString:aString];
        self.title = nil;
        
        __weak typeof(self)weakSelf = self;
        
        [self requestPageTitle:^(NSString *pageTitle) {
        
            weakSelf.title = pageTitle;
            
        } failure:^(NSError *error) {
            
            weakSelf.title = nil;
        }];
    }
    
    return self;
}

- (id)serialize
{
    return @{
             @"url" : self.url.absoluteString,
             @"title" : self.title ? self.title : @""
            };
}


#pragma mark - Private methods

- (void)requestPageTitle:(void (^)(NSString *))success failure:(void (^)(NSError *))failure
{
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithURL:self.url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSString *html = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSRange start = [html rangeOfString:@"<title>"];
        NSRange end = [html rangeOfString:@"</title>"];
        
        if (error || start.location == NSNotFound)
        {
            if (failure)
            {
                failure(error);
            }
        }
        else
        {
            NSInteger startLocation = start.location + start.length;
            NSRange titleRange = NSMakeRange(startLocation, end.location-startLocation);
            NSString *title = [html substringWithRange:titleRange];
            
            NSString *escapedString = [title gtm_stringByUnescapingFromHTML]; //unescape html entities often present in returned page titles. Such as | &  
            
            if (success)
            {
                success(escapedString);
            }
        }
    }];
    
    [dataTask resume];
}


@end
