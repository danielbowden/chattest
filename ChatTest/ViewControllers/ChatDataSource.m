//
//  ChatDataSource.m
//  ChatTest
//
//  Created by Daniel Bowden on 11/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "ChatDataSource.h"

#import "ChatMessage.h"
#import "ChatMessageTableViewCell.h"

@interface ChatDataSource ()

@property (nonatomic, copy) TableViewCellConfigureBlock configureCellBlock;
@property (nonatomic, strong) NSMutableArray<ChatMessage *> *messages;

- (void)loadDemoData;

@end

@implementation ChatDataSource

- (id)init
{
    NSAssert(NO, @"Use initWithConfigureCellBlock: instead");
    return nil;
}

- (id)initWithConfigureCellBlock:(TableViewCellConfigureBlock)aConfigureCellBlock
{
    if ((self = [super init]))
    {
        self.configureCellBlock = [aConfigureCellBlock copy];
        [self loadDemoData];
    }
    
    return self;
}

- (ChatMessage *)messageAtIndex:(NSInteger)index
{
    if (index < self.messages.count)
    {
        return self.messages[index];
    }
    
    return nil;
}

- (NSInteger)numberOfMessages
{
    return self.messages.count;
}

- (void)addNewChatMessage:(ChatMessage *)message
{
    [self.messages addObject:message];
}

#pragma mark - Private methods

- (void)loadDemoData
{
    ChatMessage *message1 = [[ChatMessage alloc] initWithMessage:@"@chris you around?"];
    ChatMessage *message2 = [[ChatMessage alloc] initWithMessage:@"Good morning! (megusta) (coffee)"];
    ChatMessage *message3 = [[ChatMessage alloc] initWithMessage:@"Olympics are starting soon; http://www.nbcolympics.com"];
    ChatMessage *message4 = [[ChatMessage alloc] initWithMessage:@"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"];
    ChatMessage *message5 = [[ChatMessage alloc] initWithMessage:@"@daniel nice chat app. (megusta) I had a look at your site http://www.danielbowden.com.au"];
    
    self.messages = [NSMutableArray arrayWithArray:@[message1, message2, message3, message4, message5]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self numberOfMessages];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ChatMessageTableViewCell cellIdentifier] forIndexPath:indexPath];
    ChatMessage *message = [self messageAtIndex:indexPath.row];
    self.configureCellBlock(cell, message);
    
    return cell;
}

@end
