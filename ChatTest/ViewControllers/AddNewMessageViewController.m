//
//  AddNewMessageViewController.m
//  ChatTest
//
//  Created by Daniel Bowden on 15/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "AddNewMessageViewController.h"

#import "ChatMessage.h"

@interface AddNewMessageViewController () <UITextViewDelegate>

@property (nonatomic, copy) AddNewMessageCompletionBlock completionBlock;
@property (nonatomic, strong) UITextView *textView;

- (void)cancelPost;
- (void)savePost;

@end

@implementation AddNewMessageViewController

- (instancetype)initWithCompletionBlock:(AddNewMessageCompletionBlock)completionBlock
{
    if ((self = [super init]))
    {
        self.completionBlock = [completionBlock copy];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"New Message";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelPost)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(savePost)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)loadView
{
    self.view = [[UIView alloc] init];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.textView = [[UITextView alloc] init];
    [self.textView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:self.textView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.textView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.textView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0
                                                           constant:0]];
    
    self.textView.textColor = [UIColor grayColor];
    self.textView.font = [UIFont systemFontOfSize:16.0];
    self.textView.returnKeyType = UIReturnKeyDone;
    self.textView.delegate = self;
    [self.textView becomeFirstResponder];
}

#pragma mark - Private methods

- (void)cancelPost
{
    if (self.completionBlock)
    {
        self.completionBlock(nil);
    }
}

- (void)savePost
{
    if (self.textView.text.length > 0)
    {
        ChatMessage *newMessage = [[ChatMessage alloc] initWithMessage:self.textView.text];
        
        if (self.completionBlock)
        {
            self.completionBlock(newMessage);
        }
    }
}


#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    self.navigationItem.rightBarButtonItem.enabled = (textView.text.length > 0);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
