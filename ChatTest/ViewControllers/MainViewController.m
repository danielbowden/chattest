//
//  MainViewController.m
//  ChatTest
//
//  Created by Daniel Bowden on 9/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "MainViewController.h"

#import "ChatMessage.h"
#import "ChatDataSource.h"
#import "MessageViewController.h"
#import "ChatMessageTableViewCell.h"
#import "AddNewMessageViewController.h"

@interface MainViewController () <UITableViewDelegate>

@property (nonatomic, strong) ChatDataSource *chatDataSource;

- (void)setupTableView;
- (void)addNewMessage;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Chat Test";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewMessage)];
    
    [self setupTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

#pragma mark - Private methods

- (void)setupTableView
{
    self.chatDataSource = [[ChatDataSource alloc] initWithConfigureCellBlock:^(ChatMessageTableViewCell *cell, ChatMessage *message) {
        [cell populateWithMessage:message];
    }];
    
    self.tableView.dataSource = self.chatDataSource;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[ChatMessageTableViewCell class] forCellReuseIdentifier:[ChatMessageTableViewCell cellIdentifier]];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
}

- (void)addNewMessage
{
    __weak typeof(self)weakSelf = self;
    
    AddNewMessageViewController *viewController = [[AddNewMessageViewController alloc] initWithCompletionBlock:^(ChatMessage *message) {
        
        if (message != nil)
        {
            [weakSelf.chatDataSource addNewChatMessage:message];
            [weakSelf.tableView reloadData];
        }
        
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:viewController] animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatMessage *message = [self.chatDataSource messageAtIndex:indexPath.row];
    MessageViewController *viewController = [[MessageViewController alloc] initWithChatMessage:message];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
