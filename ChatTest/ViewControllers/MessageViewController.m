//
//  MessageViewController.m
//  ChatTest
//
//  Created by Daniel Bowden on 12/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import "MessageViewController.h"

#import "ChatMessage.h"

@interface MessageViewController ()

@property (nonatomic, strong) ChatMessage *message;
@property (nonatomic, strong) UITextView *textView;

@end

@implementation MessageViewController

- (instancetype)initWithChatMessage:(ChatMessage *)message
{
    if ((self = [super init]))
    {
        self.message = message;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"JSON Representation";
    self.textView.text = [self.message serialize];
}

- (void)loadView
{
    self.view = [[UIView alloc] init];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.textView = [[UITextView alloc] init];
    self.textView.translatesAutoresizingMaskIntoConstraints = NO;
    self.textView.editable = NO;
    [self.view addSubview:self.textView];

    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.textView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.textView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0
                                                           constant:0]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
