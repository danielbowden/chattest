//
//  MessageViewController.h
//  ChatTest
//
//  Created by Daniel Bowden on 12/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatMessage;

@interface MessageViewController : UIViewController

- (instancetype)initWithChatMessage:(ChatMessage *)message;

@end
