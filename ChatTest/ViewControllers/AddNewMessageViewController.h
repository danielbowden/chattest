//
//  AddNewMessageViewController.h
//  ChatTest
//
//  Created by Daniel Bowden on 15/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatMessage;

typedef void (^AddNewMessageCompletionBlock)(ChatMessage *message);

@interface AddNewMessageViewController : UIViewController

- (instancetype)initWithCompletionBlock:(AddNewMessageCompletionBlock)completionBlock;

@end
