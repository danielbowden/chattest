//
//  ChatDataSource.h
//  ChatTest
//
//  Created by Daniel Bowden on 11/01/2016.
//  Copyright © 2016 Daniel Bowden. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ChatMessage;
@class ChatMessageTableViewCell;

typedef void (^TableViewCellConfigureBlock)(ChatMessageTableViewCell *cell, ChatMessage *message);

@interface ChatDataSource : NSObject <UITableViewDataSource>

- (id)initWithConfigureCellBlock:(TableViewCellConfigureBlock)aConfigureCellBlock;
- (ChatMessage *)messageAtIndex:(NSInteger)index;
- (NSInteger)numberOfMessages;
- (void)addNewChatMessage:(ChatMessage *)message;

@end
